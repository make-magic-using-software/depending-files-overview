# Depending Files Overview
Code file depending analysis. Let you have an overview of your projects.

## Background
I want to know more about React library. But lots of folders and files.   
That makes me have an idea that analyze the file depending relation. So I developed this tool.

## Repository Address
I manage it with GitHub because I want to have a GitHub try.  
[`https://github.com/ShawnYou1/depending-files-overview`](https://github.com/ShawnYou1/depending-files-overview)

## React Lib DEMO
![alt demo](https://raw.githubusercontent.com/ShawnYou1/depending-files-overview/master/src/overview/images/screenshoot.jpg)